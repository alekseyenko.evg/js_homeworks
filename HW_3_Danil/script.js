employeeArr = [
    {
      id: 1,
      name: "Денис",
      surname: "Хрущ",
      salary: 1010,
      workExperience: 10, /// стаж работы (1 = один месяц)
      isPrivileges: false, /// льготы
      gender: "male"
    },
    {
      id: 2,
      name: "Сергей",
      surname: "Войлов",
      salary: 1200,
      workExperience: 12, /// стаж работы (1 = один месяц)
      isPrivileges: false, /// льготы
      gender: "male"
    },
    {
      id: 3,
      name: "Татьяна",
      surname: "Коваленко",
      salary: 480,
      workExperience: 3, /// стаж работы (1 = один месяц)
      isPrivileges: true, /// льготы
      gender: "female"
    },
    {
      id: 4,
      name: "Анна",
      surname: "Кугир",
      salary: 2430,
      workExperience: 20, /// стаж работы (1 = один месяц)
      isPrivileges: false, /// льготы
      gender: "female"
    },
    {
      id: 5,
      name: "Татьяна",
      surname: "Капустник",
      salary: 3150,
      workExperience: 30, /// стаж работы (1 = один месяц)
      isPrivileges: true, /// льготы
      gender: "female"
    },
    {
      id: 6,
      name: "Станислав",
      surname: "Щелоков",
      salary: 1730,
      workExperience: 15, /// стаж работы (1 = один месяц)
      isPrivileges: false, /// льготы
      gender: "male"
    },
    {
      id: 7,
      name: "Денис",
      surname: "Марченко",
      salary: 5730,
      workExperience: 45, /// стаж работы (1 = один месяц)
      isPrivileges: true, /// льготы
      gender: "male"
    },
    {
      id: 8,
      name: "Максим",
      surname: "Меженский",
      salary: 4190,
      workExperience: 39, /// стаж работы (1 = один месяц)
      isPrivileges: false, /// льготы
      gender: "male"
    },
    {
      id: 9,
      name: "Антон",
      surname: "Завадский",
      salary: 790,
      workExperience: 7, /// стаж работы (1 = один месяц)
      isPrivileges: false, /// льготы
      gender: "male"
    },
    {
      id: 10,
      name: "Инна",
      surname: "Скакунова",
      salary: 5260,
      workExperience: 49, /// стаж работы (1 = один месяц)
      isPrivileges: true, /// льготы
      gender: "female"
    },
    {
      id: 11,
      name: "Игорь",
      surname: "Куштым",
      salary: 300,
      workExperience: 1, /// стаж работы (1 = один месяц)
      isPrivileges: false, /// льготы
      gender: "male"
    }
  ];
  
 
 class Emploee {
    constructor(data) {
      this.id = data.id;
      this.name = data.name;
      this.surname = data.surname;
      this.salary = data.salary;
      this.workExperience = data.workExperience;
      this.isPrivileges = data.isPrivileges;
      this.gender = data.gender;
    }
  
    get fullInfo() {
      const keys = Object.keys(this);
      return keys.map((key) => `${key} - ${this[key]}`).join(", ");
    }
  
    set fullInfo(data) {
      const keys = Object.keys(this);
      keys.forEach((key) => {
        this[key] = data[key] || this[key];
      });
    }
  }
  function Employee(employee) {
    const keys = Object.keys(employee);
    for (const key of keys) {
      this[key] = employee[key];
    }
  }
  
  const em = new Employee(employeeArr[0]);
  console.log(em);
  
  // 2
  Employee.prototype.getFullName = function () {
    return this.name + ' ' + this.surname;
  };
  console.log(em.getFullName());
  
  // 3
  const createEmployesFromArr = (arr) => {
    return arr.map((el) => new Employee(el));
  };
  const employeeConstructArr = createEmployesFromArr(employeeArr);
  console.log(employeeConstructArr);
  
  // 4
  const getFullNamesFromArr = (arr) => {
    return arr.map((el) => el.getFullName());
  };
  
  const names = getFullNamesFromArr(employeeConstructArr);
  console.log(names);
  
  // 5
  const getMiddleSalary = (arr) => {
    return (Math.floor(arr.reduce((sum, el) => sum + el.salary, 0) / arr.length));
  };
  
  const midSalary = getMiddleSalary(employeeConstructArr);
  console.log('average salary =' + ' ' + midSalary);
  
  // 6
  const getRandomEmployee = (arr) => {
    const index = Math.floor((arr.length - 1) * Math.random());
    return arr[index];
  };
  
  const randEm = getRandomEmployee(employeeConstructArr);
  console.log(randEm);
  
  // 7
  Object.defineProperty(Employee.prototype, "fullInfo", {
    get: function () {
      const keys = Object.keys(this);
      return keys.map((key) => `${key} - ${this[key]}`).join(", ");
    },
    set: function (data) {
      const keys = Object.keys(this);
      keys.forEach((key) => {
        this[key] = data[key] || this[key];
      });
    }
  });
  
  console.log(em.fullInfo);
  em.fullInfo = { name: "Вася", salary: 9000 };
  console.log(em.fullInfo);